require("./server/config/env");
const http = require("http");
const app = require("./server/config/express");

//connect to database
const { connectDBurl } = require("./server/config/database");
connectDBurl();

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.API_PORT || "5000");
app.set("port", port);

// create HTTP Server
const server = http.createServer(app);

server.listen(port, () => {
  console.info(
    `<< ${process.env.NODE_ENV} server started on >> ${process.env.API_HOST}`
  );
});

server.on("error", onError);
/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string" ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
    case "EADDRINUSE":
      console.error(`${bind} is already in use`);
      process.exit(1);
    default:
      throw error;
  }
}