const WalletHistory = require("../../models/walletHistoryModel");

const createWalletHistory = async (data) => {
    const newWalletHistory = new WalletHistory(data);
    const result = await newWalletHistory.save();
    return result;
};

const getWalletHistoryDetailsByAddress = async (address) => {
    const result = await WalletHistory.findOne({
        address,
        // createdAt: new Date()
    });
    return result;
};

const getWalletHistoryListByAddress = async (address) => {
    const result = await WalletHistory.find({
        address,
        // createdAt: new Date()
    });
    return result;
};

const getWalletHistoryDetailsByAddressByDate = async (date, address) => {
    let dateObject = {
        startDay: new Date(date).setUTCHours(0, 0, 0, 0, 0, 0),
        endDay: new Date(date).setUTCHours(23, 59, 59, 999),
    };
    const result = await WalletHistory.findOne({
        address,
        createdAt: {
            $gte: dateObject.startDay,
            $lte: dateObject.endDay,
        },
    });
    return result;
};

const updateWalletHistoryById = async (id, data) => {
    const result = await WalletHistory.findByIdAndUpdate(id, data, {
        upsert: true,
        new: true,
    });
    return result;
};

const getWalletHistoryDetails = async (id) => {
    const result = await WalletHistory.findById(id);
    return result;
};

const getWalletListMonthly = async (address) => {
    const result = await WalletHistory.aggregate([
        {
            $match: {
                address: address,
            },
        },
        {
            $sort: {
                createdAt: -1,
            },
        },
        {
            $project: {
                month: {
                    $month: "$createdAt",
                },
                address: 1,
                balance: 1,
                balanceChange: 1,
                createdAt: 1,
                initialBalance: 1,
            },
        },
        {
            $sort: {
                createdAt: 1,
            },
        },
        {
            $project: {
                month: 1,
                address: 1,
                balance: 1,
                balanceChange: 1,
                createdAt: 1,
                initialBalance: 1,
                monthlyBalanceChange: {
                    $sum: "$balanceChange",
                },
            },
        },
        {
            $group: {
                _id: "$month",
                address: {
                    $first: "$address",
                },
                month: {
                    $first: "$month"
                },
                initialBalance: {
                    $first: "$initialBalance",
                },
                sumOfBalanceChange: {
                    $sum: "$balanceChange",
                },
            },
        },
    ]);
    return result;
};

const getWalletListByWeekly = async (address) => {
    const result = await WalletHistory.aggregate([
        {
            $match: {
                address: address,
            },
        },
        {
            $sort: {
                createdAt: -1,
            },
        },
        {
            $project: {
                week: {
                    $week: "$createdAt",
                },
                address: 1,
                balance: 1,
                balanceChange: 1,
                createdAt: 1,
                initialBalance: 1,
            },
        },
        {
            $sort: {
                createdAt: 1,
            },
        },
        {
            $project: {
                week: 1,
                address: 1,
                balance: 1,
                balanceChange: 1,
                createdAt: 1,
                initialBalance: 1,
                monthlyBalanceChange: {
                    $sum: "$balanceChange",
                },
            },
        },
        {
            $group: {
                _id: "$week",
                week: {
                    $first: "$week",
                },
                address: {
                    $first: "$address",
                },
                initialBalance: {
                    $first: "$initialBalance",
                },
                sumOfBalanceChange: {
                    $sum: "$balanceChange",
                },
            },
        },
    ]);
    return result;
};

module.exports = {
    createWalletHistory,
    updateWalletHistoryById,
    getWalletHistoryDetails,
    getWalletHistoryDetailsByAddress,
    getWalletHistoryDetailsByAddressByDate,
    getWalletHistoryListByAddress,
    getWalletListMonthly,
    getWalletListByWeekly
};
