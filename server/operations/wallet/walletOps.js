const Wallet = require("../../models/walletModel");

const createWallet = async (data) => {
    const newWallet = new Wallet(data);
    const result = await newWallet.save();
    return result;
};

const getWalletDetailsByAddress = async (address) => {
    const result = await Wallet.findOne({ address });
    return result;
};

const updateWalletById = async (id, data) => {
    const result = await Wallet.findByIdAndUpdate(id, data, {
        upsert: true,
        new: true,
    });
    return result;
};

const updateWalletByAddress = async (address, balance) => {
    const result = await Wallet.findOneAndUpdate({ address }, { balance }, {
        upsert: true,
        new: true,
    });
    return result;
};

const getWalletDetails = async (id) => {
    const result = await Wallet.findById(id);
    return result;
};

module.exports = {
    createWallet,
    updateWalletById,
    getWalletDetails,
    getWalletDetailsByAddress,
    updateWalletByAddress
}