const User = require("../../models/userModel");
const mongoose = require("mongoose");

const createUser = async (data) => {
    debugger;
    const newUser = new User(data);
    const result = await newUser.save();
    debugger;
    return result;
};

const findUserByEmail = async (email) => {
    const result = await User.findOne({
        email,
        isDeleted: false,
        status: "Active",
    }).select("+password");
    return result;
};

const getUserByWalletAddress = async (walletAddress) => {
    const result = await User.findOne({
        walletAddress: {
            $in: [walletAddress],
        },
    });
    return result;
};

const findUserByIdWithPassword = async (id) => {
    const result = await User.findOne({
        _id: id,
        isDeleted: false,
        status: "Active",
    }).select("+password");
    return result;
};

const updateUserProfileById = async (id, data) => {
    const result = await User.findByIdAndUpdate(id, data, {
        upsert: true,
        new: true,
    }).exec();
    return result;
};

const getUserDetails = async (id) => {
    const result = await User.findById(id);
    return result;
};

const updateWalletAddressById = async (id, data) => {
    const result = await User.findByIdAndUpdate(
        id,
        {
            $push: {
                walletAddress: data.walletAddress,
            },
        },
        {
            upsert: true,
            new: true,
        }
    ).exec();
    return result;
};

const getWalletListById = async (id) => {
    const result = await User.aggregate([
        {
            $match: {
                _id: id,
            },
        },
        {
            $unwind: {
                path: "$walletAddress",
                includeArrayIndex: "string",
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: "wallets",
                let: {
                    pId: "$walletAddress",
                },
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: ["$_id", "$$pId"],
                            },
                        },
                    },
                    {
                        $project: {
                            address: 1,
                            account: 1,
                        },
                    },
                ],
                as: "walletAddress",
            },
        },
        {
            $unwind: {
                path: "$walletAddress",
                includeArrayIndex: "string",
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $group: {
                _id: "$_id",
                walletAddress: {
                    $push: "$walletAddress",
                },
            },
        },
    ]);
    return result[0];
};
module.exports = {
    createUser,
    findUserByEmail,
    findUserByIdWithPassword,
    updateUserProfileById,
    getUserDetails,
    updateWalletAddressById,
    getUserByWalletAddress,
    getWalletListById,
};
