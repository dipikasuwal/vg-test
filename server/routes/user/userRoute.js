const router = require("express").Router();
const userCtrl = require("../../controllers/user/userController");
const validator = require("../../validation/validator");
const { Auth } = require("../../middlewares/Auth");
const UserSchema = require("../../validation/schemas/userSchema")
router
    .route("/register")
    /**
     * @typedef RegisterUserRequest
     * @property {string} name - name: -eg: user22
     * @property {string} address -address: -eg:KTM
     * @property {string} email -email: -eg: user22@gmail.com
     * @property {string} password -password: -eg: user12345
     * @property {string} contactNumber -contactNumber: -eg: 9812345678
     * @property {string} gender -gender: -eg: Male
     * @property {number} age -age: -eg: 15
     */
    /**
     * @route POST /user/register
     * @group User
     * @operationId registerUser
     * @param {RegisterUserRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     */
    .post(
        validator.validateRequestBody(UserSchema.registerUser, "Register User"),
        userCtrl.register
    );

router
    .route("/login")
    /**
     * @typedef LoginRequest
     * @property {string} email - email: -eg: user21@getnade.com
     * @property {string} password - password: -eg: user12345
     */
    /**
     * @route POST /user/login
     * @group User
     * @operationId loginUser
     * @param {LoginRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     */
    .post(
        validator.validateRequestBody(UserSchema.loginUser, "Login User"),
        userCtrl.loginUser
    );

router
    .route("/update-details")
    /**
     * @typedef BasicProfileRequest
     * @property {string} name - name: -eg: user
     * @property {string} email - email: -eg: user21@getnade.com
     * @property {string} contactNumber - contactNumber: -eg: 9811111111
     * @property {string} gender - gender: -eg: Male
     * @property {integer} age - age: -eg: 20
     */
    /**
     * @route PUT /user/update-details
     * @group User
     * @operationId updateDetailsOfUser
     * @param {BasicProfileRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .put(
        validator.validateRequestBody(UserSchema.updateUser, "Update User"),
        Auth,
        userCtrl.updateDetailsOfUser
    );

router
    .route("/wallet-address")
    /**
     * @typedef AddWalletRequest
     * @property {string} walletAddress - walletAddress: -eg: 6519a9d22387d7bd77a98fc4
     */
    /**
     * @route PUT /user/wallet-address
     * @group User
     * @operationId addWalletInUser
     * @param {AddWalletRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .put(
        validator.validateRequestBody(UserSchema.addWalletAddress, "addWalletAddress"),
        Auth,
        userCtrl.addWalletInUser
    );

router
    .route("/remove-wallet-address")
    /**
     * @typedef RemoveWalletRequest
     * @property {string} walletAddress - walletAddress: -eg: 6519a9d22387d7bd77a98fc4
     */
    /**
     * @route PUT /user/remove-wallet-address
     * @group User
     * @operationId removeWalletFromUser
     * @param {RemoveWalletRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .put(
        validator.validateRequestBody(UserSchema.addWalletAddress, "addWalletAddress"),
        Auth,
        userCtrl.removeWalletFromUser
    );

router
    .route("/update-wallet-address")
    /**
     * @typedef UpdateWalletRequest
     * @property {string} oldWalletAddress - oldWalletAddress: -eg: 6519a9d22387d7bd77a98fc4
     * @property {string} newWalletAddress - newWalletAddress: -eg: 6519a9d22387d7bd77a98fca
     */
    /**
     * @route PUT /user/update-wallet-address
     * @group User
     * @operationId updateWalletFromUser
     * @param {UpdateWalletRequest.model} body.body.required
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .put(
        validator.validateRequestBody(UserSchema.updateWalletAddress, "updateWalletAddress"),
        Auth,
        userCtrl.updateWalletFromUser
    );

router
    .route("/wallet-address-list")
    /**
     * @route GET /user/wallet-address-list
     * @group User
     * @operationId getWalletListOfUser
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .get(
        Auth,
        userCtrl.getWalletListOfUser
    );

router
    .route("/balance-change-daily/:walletId")
    /**
     * @route GET /user/balance-change-daily/{walletId}
     * @group User
     * @operationId getWalletHistoryOfUserDaily
     * @param {id} walletId.path - eg: 5e2583b17e234e3352723427
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .get(
        validator.validateRequestParams(UserSchema.idSchema, "walletId"),
        Auth,
        userCtrl.getWalletHistoryOfUserDaily
    );

router
    .route("/balance-change-monthly/:walletId")
    /**
     * @route GET /user/balance-change-monthly/{walletId}
     * @group User
     * @operationId getWalletHistoryOfUserMonthly
     * @param {id} walletId.path - eg: 5e2583b17e234e3352723427
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .get(
        validator.validateRequestParams(UserSchema.idSchema, "walletId"),
        Auth,
        userCtrl.getWalletHistoryOfUserMonthly
    );

router
    .route("/balance-change-weekly/:walletId")
    /**
     * @route GET /user/balance-change-weekly/{walletId}
     * @group User
     * @operationId getWalletHistoryOfUserWeekly
     * @param {id} walletId.path - eg: 5e2583b17e234e3352723427
     * @produces application/json
     * @consumes application/json
     * @returns {string} 200 -success
     * @returns {string} 409 - Conflict
     * @returns {string} 422 - Unprocessable Entity
     * @security JWT
     */
    .get(
        validator.validateRequestParams(UserSchema.idSchema, "walletId"),
        Auth,
        userCtrl.getWalletHistoryOfUserWeekly
    );

module.exports = router;
