const router = require("express").Router();

const userRoute = require("../routes/user/userRoute");

router.use("/user", userRoute);

module.exports = router;
