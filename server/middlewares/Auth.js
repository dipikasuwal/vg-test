const jwt = require("jsonwebtoken");
const User = require("../models/userModel");
const resHelp = require("../helpers/responseHelper");
const tokenHelp = require("../helpers/tokenHelper");

const JWT_SECRET = process.env.JWT_SECRET;

const Auth = async (req, res, next) => {
    if (req.headers['authorization']) {
        const jwtVerify = await jwt.verify(
            req.headers['authorization'].split(" ")[1],
            JWT_SECRET,
            async (err, decoded) => {
                if (err) {
                    return resHelp.respondError(res, 401, "", "Invalid token");
                } else {
                    let user = await User.findOne({ _id: decoded._id, isDeleted: false });
                    if (!user) {
                        return resHelp.respondError(res, 401, "", "Account Not Found");
                    }
                    req.user = user;
                    next();
                }
            }
        )
    }
}

module.exports = {
    Auth
}
