var cron = require("node-cron");
const { getWalletsWithBalance } = require("../services/bscan");

const scheduleToGetBalance = () => {
    cron.schedule(
        "*/5 * * * *",
        async () => {
            // console.log("5min", new Date())
            const schedule = await getWalletsWithBalance();
        },
        {
            scheduled: true,
            timezone: "Asia/Kathmandu",
        }
    );
}
module.exports = {
    scheduleToGetBalance,
}