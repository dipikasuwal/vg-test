const https = require("https");
const walletOps = require("../operations/wallet/walletOps")
const walletHistoryOps = require("../operations/walletHistory/walletHistoryOps")

const getWalletsWithBalance = async () => {
    const apiKey = process.env.API_KEY;

    let addresses = process.env.ADDRESSES;
    addresses = addresses.split(",")

    let options = {
        hostname: "api-testnet.bscscan.com",
        path: `/api?module=account&action=balancemulti&address=${addresses[0]},${addresses[1]},${addresses[2]}&tag=latest&apikey=${apiKey}`,
        method: "GET",
        headers: {
            Accept: "application/json",
        },
    };

    let requestService = https.request(options, (response) => {
        response.on("data", async (d) => {
            let responseData = JSON.parse(d.toString("utf8"));
            if (!responseData) {
                //error
                console.log({ responseData });
            } else {
                //success
                const walletList = responseData.result.map(async (item) => {
                    item.address = item.account;

                    const getWalletDetails = await walletOps.getWalletDetailsByAddress(item.address);
                    if (!getWalletDetails) {
                        const addWalletInDb = await walletOps.createWallet(item);
                    } else {
                        const updateWallet = await walletOps.updateWalletByAddress(getWalletDetails.address, item.balance);
                        const getWalletHistory = await walletHistoryOps.getWalletHistoryDetailsByAddressByDate(new Date(), getWalletDetails.address);
                        if (!getWalletHistory) {
                            let walletDetailsObject = getWalletDetails.toObject();
                            delete walletDetailsObject._id;
                            walletDetailsObject.initialBalance = walletDetailsObject.balance;
                            const newWalletHistory = await walletHistoryOps.createWalletHistory(walletDetailsObject);
                            console.log("create")
                        } else {
                            console.log("update")
                            let getWalletHistoryObject = getWalletHistory.toObject();
                            const updateToWalletHistory = {
                                initialBalance: getWalletHistoryObject.balance,
                                balance: getWalletDetails.balance,
                                balanceChange: Math.abs(getWalletHistoryObject.balance - getWalletDetails.balance)
                            }
                            const updateWalletHistory = await walletHistoryOps.updateWalletHistoryById(getWalletHistory._id, updateToWalletHistory)
                        }
                    }
                })
                return walletList;
            }
        });
    });
    requestService.on("error", (error) => {
        //error
        console.log({ error });
    });
    requestService.end();
};
module.exports = {
    getWalletsWithBalance,
};
