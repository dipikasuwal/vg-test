const GLOBALVARS = {
  successStatusCode: 200,
  errorStatusCode: 422,
  unAuthorizedStatusCode: 401,
  notFound: 404,
  forbiddenStatusCode: 403,
  gone: 410,
  contactNumberNotVerified: 446,
  refreshTokenExpired: 440,
};

module.exports = GLOBALVARS;
