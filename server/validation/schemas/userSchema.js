const Joi = require("joi");
Joi.objectId = require("joi-objectid")(Joi);

const registerUser = Joi.object().keys({
    name: Joi.string().required().label("Full Name"),
    email: Joi.string().email().required().label("Email"),
    address: Joi.string().required().label("address"),
    password: Joi.string().required().label("password"),
    contactNumber: Joi.string().required().min(10).label("contactNumber"),
    gender: Joi.string().required().valid("Male", "Female", "Others").label("gender"),
    age: Joi.number().required().min(1).label("age"),

});

const loginUser = Joi.object().keys({
    email: Joi.string().email().required().label("Email"),
    password: Joi.string().required().label("password"),
});

const updateUser = Joi.object().keys({
    name: Joi.string().required().label("Full Name"),
    email: Joi.string().email().required().label("Email"),
    address: Joi.string().required().label("address"),
    password: Joi.string().required().label("password"),
    contactNumber: Joi.string().required().min(10).label("contactNumber"),
    gender: Joi.string().required().valid("Male", "Female", "Others").label("gender"),
    age: Joi.number().required().min(1).label("age"),

});

const addWalletAddress = Joi.object().keys({
    walletAddress: Joi.objectId().required().label("walletAddress"),
})

const updateWalletAddress = Joi.object().keys({
    oldWalletAddress: Joi.objectId().required().label("oldWalletAddress"),
    newWalletAddress: Joi.objectId().required().label("newWalletAddress"),
})

const idSchema = Joi.object().keys({
    walletId: Joi.objectId().required().label("walletId"),
});

module.exports = {
    updateUser,
    addWalletAddress,
    updateWalletAddress,
    registerUser,
    loginUser,
    idSchema
};
