const express = require("express");
const path = require("path");
const fs = require("fs");
const app = express();
const logger = require("morgan");
const bodyParser = require("body-parser");
const routes = require("../routes/indexRoute");

// const { getWalletsWithBalance } = require("../services/bscan");
// getWalletsWithBalance();

const { scheduleToGetBalance } = require("../services/cronService");
scheduleToGetBalance();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const swaggerUiGenerator = require("express-swagger-generator");
const expressSwager = swaggerUiGenerator(app);
const options = {
  swaggerDefinition: {
    info: {
      description: "VG-Test API Documentation",
      title: "VG",
      version: "1.0.0",
    },
    host: `${process.env.API_HOST}`,
    basePath: "/api",
    produces: ["application/json"],
    schemes: `${process.env.API_HOST.includes("localhost") ? "http" : "https"}`,
    securityDefinitions: {
      JWT: {
        type: "apiKey",
        in: "header",
        name: "Authorization",
        description: "JWT Token",
      },
    },
  },
  basedir: __dirname, // app absolute path
  files: ["./../routes/*/*.js"], // Path to the API handle folder
};

if (process.env.NODE_ENV === "development") {
  app.use(logger("dev"));
  expressSwager(options);
}
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    // res.header("Access-Control-Expose-Headers", "accessToken");
    res.header("Access-Control-Allow-Methods", "PATCH, PUT, POST, DELETE, GET");
    return res.status(200).json({});
  }
  if (["PATCH", "POST", "PUT"].includes(req.method)) {
    if (req.is("application/json") || req.is("multipart/form-data")) {
      return next();
    } else {
      res
        .status(422)
        .send({
          title: "Content Type",
          message: "Content Type not Accepted!",
        })
        .end();
    }
  } else if (["DELETE", "GET"].includes(req.method)) {
    return next();
  } else {
    res
      .status(422)
      .send({
        title: "Methods",
        message: "Method not allowed!",
      })
      .end();
  }
});

app.use("/api/", routes);
app.use("*", (req, res) => {
  res
    .status(422)
    .send({
      title: "Api",
      message: "Api path not found",
    })
    .end();
});

//socket
// var io = socketApi.socketApi.io;
// // io.set('origins', '*');
// io.attach(app)
// io.attach(server, {
//   cors: {
//     origin: 
//   }
// });

app.use((err, req, res, next) => {
  let today = new Date();
  let year = today.getUTCFullYear().toString();
  let month = (today.getUTCMonth() + 1).toString();
  let day = today.getUTCDate().toString();

  let folderPath = path.join(__dirname, "..", "..", "logs", year, month);
  let filePath = path.join(folderPath, day + ".log");

  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath, {
      recursive: true,
    });
  }

  fs.appendFile(
    filePath,
    today.toISOString() + "-" + err + "\n",
    {
      flag: "a+",
    },
    (err) => {
      if (err) {
        //Error while trying to write to file
      }
    }
  );
  next();
});

module.exports = app;
