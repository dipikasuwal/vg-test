const respondSuccess = async (res, statusCode, title, message, data = {}) => {
    res
        .status(statusCode)
        .send({
            title,
            message,
            data,
        })
        .end();
};

const respondError = async (res, statusCode, title, message) => {
    res
        .status(statusCode)
        .send({
            title,
            message,
        })
        .end();
};

module.exports = {
    respondSuccess,
    respondError,
};
