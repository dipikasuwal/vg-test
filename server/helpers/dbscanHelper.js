const walletHistoryOps = require("../operations/walletHistory/walletHistoryOps")

const getDailyChangeWallet = async (data) => {
    const walletDetails = await walletHistoryOps.getWalletHistoryListByAddress(data);
    if (walletDetails) {
        const walletWithAmountAndPercentage = [];
        const wallet = walletDetails.map(async (item) => {
            var percentageOfBalanceChange = 0;
            if (item.balanceChange == 0) {
                percentageOfBalanceChange = 0;
            } else {
                percentageOfBalanceChange = (item.balanceChange / item.initialBalance) * 100;
            }
            let itemObject = item.toObject();
            itemObject.changeAmount = item.balanceChange;
            itemObject.percentage = percentageOfBalanceChange.toFixed(5);
            walletWithAmountAndPercentage.push(itemObject);
        })
        return walletWithAmountAndPercentage;
    };
}

const getMonthlyBalanceChange = async (address) => {
    const walletDetails = await walletHistoryOps.getWalletListMonthly(address);
    if (walletDetails.length > 0) {
        const walletWithAmountAndPercentage = [];
        for (let i = 0; i < walletDetails.length; i++) {
            var percentageOfBalanceChange = 0;
            if (walletDetails[i].sumOfBalanceChange == 0) {
                percentageOfBalanceChange = 0
            } else {
                percentageOfBalanceChange = (walletDetails[i].sumOfBalanceChange * 1 / walletDetails[i].initialBalance * 1) * 100;
            }
            const dataToUpdate = {
                month: walletDetails[i].month,
                changeAmount: walletDetails[i].sumOfBalanceChange,
                percentage: percentageOfBalanceChange.toFixed(5)
            }
            walletWithAmountAndPercentage.push(dataToUpdate)
        }
        return walletWithAmountAndPercentage;
    };
}

const getWeeklyBalanceChange = async (address) => {
    const walletDetails = await walletHistoryOps.getWalletListByWeekly(address);
    if (walletDetails.length > 0) {
        const walletWithAmountAndPercentage = [];
        for (let i = 0; i < walletDetails.length; i++) {
            var percentageOfBalanceChange = 0;
            if (walletDetails[i].sumOfBalanceChange == 0) {
                percentageOfBalanceChange = 0
            } else {
                percentageOfBalanceChange = (walletDetails[i].sumOfBalanceChange * 1 / walletDetails[i].initialBalance * 1) * 100;
            }
            const dataToUpdate = {
                week: walletDetails[i].week,
                changeAmount: walletDetails[i].sumOfBalanceChange,
                percentage: percentageOfBalanceChange.toFixed(5)
            }
            walletWithAmountAndPercentage.push(dataToUpdate)
        }
        return walletWithAmountAndPercentage;
    };
}

module.exports = {
    getDailyChangeWallet,
    getMonthlyBalanceChange,
    getWeeklyBalanceChange
}