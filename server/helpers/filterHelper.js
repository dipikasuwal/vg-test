const manageSortOption = (options) => {
    if (!options.sort) {
        options.sort = "createdAt";
    }
    if (!options.dir) {
        options.dir = -1;
    } else {
        options.dir = options.dir === "desc" ? -1 : 1;
    }
    options.page = options.page ? parseInt(options.page) : 1;
    options.limit = options.limit ? parseInt(options.limit) : 10;
    options.search = options.search || "";
    options.id = options.id || "";
    options.min = options.min || 0;
    options.max = options.max || 0;

    return options;
};

module.exports = {
    manageSortOption,
};