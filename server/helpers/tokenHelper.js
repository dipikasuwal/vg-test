const jwt = require("jsonwebtoken");
const TokenModel = require("../models/tokenModel")

const JWTSECRET = process.env.JWT_SECRET;
const ACCESSTOKENEXPIREIN = process.env.ACCESS_TOKEN_EXPIRE_IN;

const REFRESHJWTSECRET = process.env.REFRESH_JWT_SECRET;
const REFRESHTOKENEXPIREIN = process.env.REFRESH_TOKEN_EXPIRE_IN;

const generateToken = async (tokenData) => {
    const timeObject = new Date();
    const refreshTokenExpire = await new Date(timeObject.getTime + REFRESHTOKENEXPIREIN * 1000);

    const accessToken = jwt.sign(tokenData, JWTSECRET, {
        expiresIn: ACCESSTOKENEXPIREIN
    });

    const refreshToken = jwt.sign(tokenData, REFRESHJWTSECRET, {
        expiresIn: REFRESHTOKENEXPIREIN
    });
    const updateToken = await TokenModel.findOneAndUpdate(
        {
            userId: tokenData._id
        },
        {
            $set: {
                accessToken,
                refreshToken
            }
        },
        {
            upsert: true,
            new: true
        }
    ).exec();
    return { accessToken, refreshToken }
}

module.exports = {
    generateToken
}