const mongoose = require("mongoose");
require('mongoose-double')(mongoose)

const WalletHistorySchema = new mongoose.Schema(
    {
        // userId: {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: "User",
        // },
        address: {
            type: String,
            trim: true
        },
        initialBalance: {
            type: mongoose.Schema.Types.Double,
            trim: true
        },
        balance: {
            type: mongoose.Schema.Types.Double,
            trim: true
        },
        balanceChange: {
            type: mongoose.Schema.Types.Double,
            trim: true,
            default: 0
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model("WalletHistory", WalletHistorySchema);