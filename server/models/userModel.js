const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const saltRounds = parseInt(process.env.PASSWORD_SALT);

const UserSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            trim: true,
            required: false,
        },
        email: {
            type: String,
            trim: true,
            required: false,
        },
        password: {
            type: String,
            trim: true,
            required: false,
        },
        contactNumber: {
            type: String,
            trim: true,
            required: false,
        },
        gender: {
            type: String,
            enum: ['Male', 'Female', 'Others'],
            trim: true,
            required: false,
        },
        age: {
            type: Number,
            min: 1,
            required: false,
        },
        walletAddress: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: "Wallet",
        },],
        isDeleted: {
            type: Boolean,
            default: false
        },
    }
);

UserSchema.pre("save", function (next) {
    if (!this.isModified("password")) {
        //passsword is not modified
        return next();
    } else {
        // password need to bcrypt before saving
        this.password = bcrypt.hashSync(this.password, saltRounds);
        return next();
    }
});

UserSchema.methods.comparePassword = async (pass, hash) => {
    const result = await bcrypt.compare(pass, hash);
    return result;
};

module.exports = mongoose.model("User", UserSchema)