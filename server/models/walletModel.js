const mongoose = require("mongoose");
require('mongoose-double')(mongoose)

const WalletSchema = new mongoose.Schema(
    {
        // userId: {
        //     type: mongoose.Schema.Types.ObjectId,
        //     ref: "User",
        // },
        address: {
            type: String,
            trim: true
        },
        balance: {
            type: mongoose.Schema.Types.Double,
            trim: true
        },
        isDeleted: {
            type: Boolean,
            default: false
        }
    },
    { timestamps: true }
)

module.exports = mongoose.model("Wallet", WalletSchema);