const mongoose = require("mongoose");

const TokenSchema = new mongoose.Schema(
    {
        userId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
        },
        refreshToken: {
            type: String,
            trim: true
        },
        accessToken: {
            type: String,
            trim: true
        },
    },
    { timestamps: true }
)

module.exports = mongoose.model("Token", TokenSchema);