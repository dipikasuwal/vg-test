const GLOBALVARS = require("../../constants/globalConstant");
const filterHelp = require("../../helpers/filterHelper");
const resHelp = require("../../helpers/responseHelper");
const tokenHelper = require("../../helpers/tokenHelper")
const dbscanHelp = require("../../helpers/dbscanHelper");

const userOps = require("../../operations/user/userOps");
const walletOps = require("../../operations/wallet/walletOps")

const register = async (req, res, next) => {
    let data = req.body;
    debugger
    const existenceCheck = await userOps.findUserByEmail(
        data.email
    );
    if (existenceCheck) {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "Register",
            "Email Already Taken"
        );
    } else {
        await userOps
            .createUser(data)
            .then(async (user) => {
                if (!user) {
                    resHelp.respondError(
                        res,
                        GLOBALVARS.errorStatusCode,
                        "Register",
                        "Failed to Register"
                    );
                } else {
                    resHelp.respondSuccess(
                        res,
                        GLOBALVARS.successStatusCode,
                        "Register",
                        "Successfully signed up"
                    );
                }
            })
            .catch((e) => next(e));
    }
};

const loginUser = async (req, res, next) => {
    let data = req.body;
    await userOps
        .findUserByEmail(data.email)
        .then(async (user) => {
            if (!user) {
                resHelp.respondError(
                    res,
                    GLOBALVARS.errorStatusCode,
                    "Login",
                    "Email is not registered"
                );
            } else {
                const isPasswordMatch = await user.comparePassword(data.password, user.password)
                if (!isPasswordMatch) {
                    resHelp.respondError(
                        res,
                        GLOBALVARS.errorStatusCode,
                        "Login",
                        "Wrong Credentials"
                    );
                } else {
                    const userObject = user.toObject();
                    delete userObject.password;

                    const tokenData = {
                        _id: userObject._id,
                        email: userObject.email
                    }
                    const token = await tokenHelper.generateToken(tokenData);
                    userObject.accessToken = `${token.accessToken}`;
                    userObject.refreshToken = `${token.refreshToken}`;

                    resHelp.respondSuccess(
                        res,
                        GLOBALVARS.successStatusCode,
                        "Login",
                        "Successfully login",
                        userObject
                    );
                }
            }
        })
        .catch((e) => next(e));
};

const updateDetailsOfUser = async (req, res, next) => {
    const data = req.body;
    const id = req.user._id;
    debugger
    await userOps.updateUserProfileById(id, data)
        .then((result) => {
            if (!result) {
                resHelp.respondError(
                    res,
                    GLOBALVARS.errorStatusCode,
                    "User Details",
                    "Failed to update"
                );
            } else {
                resHelp.respondSuccess(
                    res,
                    GLOBALVARS.successStatusCode,
                    "User Details",
                    "Successfully updated",
                    result
                );
            }
        })
}

const addWalletInUser = async (req, res, next) => {
    const data = req.body;
    const id = req.user._id;
    const checkIfWalletAlreadyAdded = await userOps.getUserByWalletAddress(data.walletAddress);
    if (checkIfWalletAlreadyAdded) {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Already assigned to others"
        );
    } else {
        await userOps.updateWalletAddressById(id, data)
            .then((result) => {
                if (!result) {
                    resHelp.respondError(
                        res,
                        GLOBALVARS.errorStatusCode,
                        "User Details",
                        "Failed to update"
                    );
                } else {
                    resHelp.respondSuccess(
                        res,
                        GLOBALVARS.successStatusCode,
                        "User Details",
                        "Successfully updated",
                        result
                    );
                }
            })
    }
};

const getWalletListOfUser = async (req, res, next) => {
    const id = req.user._id;
    await userOps.getWalletListById(id)
        .then((result) => {
            if (!result) {
                resHelp.respondError(
                    res,
                    GLOBALVARS.errorStatusCode,
                    "User Details",
                    "Failed to get"
                );
            } else {
                resHelp.respondSuccess(
                    res,
                    GLOBALVARS.successStatusCode,
                    "User Details",
                    "Successfully got",
                    result
                );
            }
        })
};

const removeWalletFromUser = async (req, res, next) => {
    const data = req.body;
    const userDetails = req.user;
    const index = userDetails.walletAddress.indexOf(data.walletAddress);
    if (index > -1) {
        userDetails.walletAddress.splice(index, 1);
        await userOps.updateUserProfileById(userDetails._id, userDetails)
            .then((result) => {
                if (!result) {
                    resHelp.respondError(
                        res,
                        GLOBALVARS.errorStatusCode,
                        "User Details",
                        "Failed to remove"
                    );
                } else {
                    resHelp.respondSuccess(
                        res,
                        GLOBALVARS.successStatusCode,
                        "User Details",
                        "Successfully removed",
                        result
                    );
                }
            })
    } else {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Wallet is not added in user"
        );
    }
}

const updateWalletFromUser = async (req, res, next) => {
    const data = req.body;
    const userDetails = req.user;
    const index = userDetails.walletAddress.indexOf(data.oldWalletAddress);
    if (index > -1) {
        userDetails.walletAddress[index] = data.newWalletAddress;
        await userOps.updateUserProfileById(userDetails._id, userDetails)
            .then((result) => {
                if (!result) {
                    resHelp.respondError(
                        res,
                        GLOBALVARS.errorStatusCode,
                        "User Details",
                        "Failed to remove"
                    );
                } else {
                    resHelp.respondSuccess(
                        res,
                        GLOBALVARS.successStatusCode,
                        "User Details",
                        "Successfully removed",
                        result
                    );
                }
            })
    } else {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Wallet is not added in user"
        );
    }
}

const getWalletHistoryOfUserDaily = async (req, res, next) => {
    // const data = req.body;
    const walletAddress = req.params.walletId;
    const userDetails = req.user;
    const index = userDetails.walletAddress.indexOf(walletAddress);
    if (index > -1) {
        const walletDetails = await walletOps.getWalletDetails(walletAddress);
        if (walletDetails) {
            await dbscanHelp.getDailyChangeWallet(walletDetails.address)
                .then((result) => {
                    if (!result) {
                        resHelp.respondError(
                            res,
                            GLOBALVARS.errorStatusCode,
                            "User Details",
                            "Failed to get"
                        );
                    } else {
                        resHelp.respondSuccess(
                            res,
                            GLOBALVARS.successStatusCode,
                            "User Details",
                            "Successfully got",
                            result
                        );
                    }
                })
        } else {
            resHelp.respondError(
                res,
                GLOBALVARS.errorStatusCode,
                "User Details",
                "Wallet is not found"
            );
        }
    } else {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Wallet is not added in user"
        );
    }
}

const getWalletHistoryOfUserMonthly = async (req, res, next) => {
    const walletAddress = req.params.walletId;
    const userDetails = req.user;
    const index = userDetails.walletAddress.indexOf(walletAddress);
    if (index > -1) {
        const walletDetails = await walletOps.getWalletDetails(walletAddress);
        if (walletDetails) {
            await dbscanHelp.getMonthlyBalanceChange(walletDetails.address)
                .then((result) => {
                    if (!result) {
                        resHelp.respondError(
                            res,
                            GLOBALVARS.errorStatusCode,
                            "User Details",
                            "Failed to get"
                        );
                    } else {
                        resHelp.respondSuccess(
                            res,
                            GLOBALVARS.successStatusCode,
                            "User Details",
                            "Successfully got",
                            result
                        );
                    }
                })
        } else {
            resHelp.respondError(
                res,
                GLOBALVARS.errorStatusCode,
                "User Details",
                "Wallet is not found"
            );
        }
    } else {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Wallet is not added in user"
        );
    }
}

const getWalletHistoryOfUserWeekly = async (req, res, next) => {
    const walletAddress = req.params.walletId;
    const userDetails = req.user;
    const index = userDetails.walletAddress.indexOf(walletAddress);
    if (index > -1) {
        const walletDetails = await walletOps.getWalletDetails(walletAddress);
        if (walletDetails) {
            await dbscanHelp.getWeeklyBalanceChange(walletDetails.address)
                .then((result) => {
                    if (!result) {
                        resHelp.respondError(
                            res,
                            GLOBALVARS.errorStatusCode,
                            "User Details",
                            "Failed to get"
                        );
                    } else {
                        resHelp.respondSuccess(
                            res,
                            GLOBALVARS.successStatusCode,
                            "User Details",
                            "Successfully got",
                            result
                        );
                    }
                })
        } else {
            resHelp.respondError(
                res,
                GLOBALVARS.errorStatusCode,
                "User Details",
                "Wallet is not found"
            );
        }
    } else {
        resHelp.respondError(
            res,
            GLOBALVARS.errorStatusCode,
            "User Details",
            "Wallet is not added in user"
        );
    }
}

module.exports = {
    register,
    loginUser,
    updateDetailsOfUser,
    addWalletInUser,
    getWalletListOfUser,
    removeWalletFromUser,
    updateWalletFromUser,
    getWalletHistoryOfUserDaily,
    getWalletHistoryOfUserMonthly,
    getWalletHistoryOfUserWeekly
}